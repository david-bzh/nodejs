# Log Entry

- Title - Text
- Description - Text
- Comments - Text
- Image - Text - URL
- Rating - scale of 1 - 10
- Latitude - Number
- Longitude - Number
- visitDate - Date


# Exemple data
[
  {
    "rating": 4,
    "title": "Empire state building",
    "comments": "Visited the top of the Empire state building. The view was great!",
    "latitude": 40.7484003,
    "longitude": -73.9856644,
    "image": "https://lh3.ggpht.com/p/AF1QipM5k9iY6RX-6s25bINUUtRMl8iuQ6nk4-_f722I",
    "visitDate": "2020-03-26T22:33:56.545Z"
  },
  {
    "rating": 5,
    "title": "Taj Mahal",
    "comments": "It is a beautiful place in the morning and in the evening !",
    "latitude": 27.1751448,
    "longitude": 78.0421422,
    "image": "https://web.static-rmg.be/if/c_crop,w_2000,h_1329,x_0,y_0,g_center/c_fit,w_620,h_411/887ec69ae5e80341e6570c9aae811a8e.jpg",
    "visitDate": "2020-03-26T22:33:56.545Z"
  },
  {
    "rating": 5,
    "title": "Zhangye Danxia Mountains",
    "comments": "Province of China, landscapes with exceptional colors !",
    "latitude": 38.9177535,
    "longitude": 100.3312601,
    "image": "https://cdn.generationvoyage.fr/2014/10/lieux-surrealistes-visiter-avant-mourir-12.jpg",
    "visitDate": "2020-03-26T22:33:56.545Z"
  }
]