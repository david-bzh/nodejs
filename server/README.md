# Install Server Node with server.js

## Description : Powerful server for Node.js

---
### Dependencies :
- Express : 4.17.1
- Cors : 2.8.5 
- Helmet : 3.21.3
- Morgan : 1.10.0
- Mongoose : 5.9.5
- Dotenv : 8.2.0
